<?php
/**
 * Tine 2.0
 *
 * @package     Bugs
 * @author      Cornelius Weiss <c.weiss@metaways.de>
 * @copyright   Copyright (c) 2009-2011 Metaways Infosystems GmbH (http://www.metaways.de)
 * @version     $Id: bugreport.php 184 2011-02-11 09:25:01Z p.schuele@metaways.de $
 */

/* Mail variables */
//from_mail and from_name are used only when an error occurs in receipt of data by GET then uses this default email
$from_mail = "xxx@xxx";
$from_name = "xxx";
//email destination
$to_mail   = "xxxx@xxxxx";
//receiver name
$to_name   = "xxx";
$mail_server = 'xxxxx@xxxx';
//ip of smtp server
$smtp_server = 'xx.xx.xx.xx';
//port from smtp server
$smtp_port = xx;
//path of zend framework libraries folder
$path = '/xxx/library/';


//map tinebase version ids to a number of known installations
$tinebase_version_id_to_installation = array(
       '1'                                        => 'Metaways Officespot',
       'a12269927bcdffd3133f9371c22222237260cb95' => 'jfischer development',
    );
/* environment vars */

set_include_path(get_include_path() . PATH_SEPARATOR . $path);

$debug = FALSE;
saveRecordToFile('teste');
if ($debug) {
    echo 'debug mode<br>';
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
}

/* Decode and handle data received via GET parameter */
$chunk = base64_decode($_GET['data']);
list($hash, $part, $data) = explode('&', $chunk);

$hash = substr($hash, 5);
$partData = substr($part, 5);
list($part, $totalParts) = explode('/', $partData);
$data = substr($data, 5);

session_id($hash);
session_start();
$_SESSION['partsdata'][$part] = $data;

if ($debug) {
    echo $chunk;
    echo $partData;
}

/* Re-assemble complete data set from received parts */

if (count($_SESSION['partsdata']) == $totalParts || $debug) {
	
    $completeData = '';
	
    for ($i=0; $i<=$totalParts; $i++) {
        $completeData .= $_SESSION['partsdata'][$i];
	}
	
    if ($debug) {
        echo $completeData . '<br>';
    }
	
    $data = decodeData($completeData);
	
    if ($debug) {
        print_r($data);
    }
	
    if ($data) {
		$subject = $data['msg'];
		$bodytext = "<pre>" . print_r($data, true) . "</pre>";
	} else {
		
		
		
		$temp = $_REQUEST;
		$temp['browser'] = $_SERVER['HTTP_USER_AGENT'];
        $temp['ip']= $_SERVER['REMOTE_ADDR'];
		$temp['time'] = Zend_Date::now()->toString('YYYY-MM-dd HH:mm:ss');
		$temp['status'] = 0;
		$temp['resolution'] = 0;
		
		$subject = "Decoding error";
		$bodytext = $completeData;
		$bodytext .= "<br><br>";
		$bodytext .= "<pre>" . print_r($temp, true) . "</pre>";
	}
	
	if ($debug) {
	    echo $bodytext;
	}
	
	// contact information may be provided as freely entered text
	// try to extract email address and name:
	if (!empty($data['contact'])) {
	   $emailValidator = new Zend_Validate_EmailAddress();
	   $contactChunks = explode(' ', $data['contact']);
	   foreach ($contactChunks as $index => $contactChunk) {
            if($emailValidator->isValid($contactChunk)) {
                unset($contactChunks[$index]);
                $from_mail = $contactChunk;
                $from_name = $contactChunk;
                break;
            }
	   }
	   if (count($contactChunks)) {
            $from_name_contact  = join(' ', $contactChunks);
       }
    }
    
    //Generate "category" to include in subject
    $subject_category = null; 
    if (!empty($data['tinebaseVersion']['id']) &&
        null !== ($installation = getInstallationByTinebaseVersionId($data['tinebaseVersion']['id']))  
       ) {
        $subject_category = $installation;
    } elseif (!empty($data['clientVersion']['packageString']) && 'none' !== $data['clientVersion']['packageString']) {
    	$subject_category = $data['clientVersion']['packageString'];
    }
    elseif (!empty($data['serverVersion']['codeName'])) {
        $subject_category = $data['serverVersion']['codeName'];
    }
    $subject = $subject_category ? "[$subject_category] $subject" : $subject; 

	mailReport($subject, $bodytext);
}

function decodeData($line)
{
    global $debug;
   
    if ($debug) {
        echo 'init autoload<br>';
    }
    
    require_once 'Zend/Loader.php';
	
    if ($debug) {
        echo 'loader loaded<br>';
    }
    Zend_Loader::registerAutoload();
	
    if (! empty($line)) {
        $data = (array)Zend_Json::decode($line);
        // sometimes data are in a different charset?!? 
        if(empty($data)) {
            $line = utf8_encode($line);
            $data = Zend_Json::decode($line);
        }
        
        // shit!!!
        if(empty($data)) {
            return 0;
        }
			
        $data = array_merge($data, (array)$data['msg']);
	
        //lets keep the trace in a data representation for later operations
        $data['trace'] = Zend_Json::encode($data['trace']);
        
        // if the message is html, we don't have a trace
        if ($data['msg']{0} == '<') {
            $data['traceHTML'] = $data['msg'];
            $data['msg'] = getMsgFromHTML($data['traceHTML']);
        }
        
        // only take first line, if we still have a multiline msg
        $data['msg'] = explode("\n", $data['msg']);
        $data['msg'] = $data['msg'][0];
        
        $data = array_merge($data, array(
            'browser'      => $_SERVER['HTTP_USER_AGENT'],
            'ip'           => $_SERVER['REMOTE_ADDR'],
            'time'         => Zend_Date::now()->toString('YYYY-MM-dd HH:mm:ss'),
            'status'       => 0,
            'resolution'   => 0
        ));
		
		return $data;
		
    } 
} 

function mailReport($subject, $bodytext)
{
	require_once 'Zend/Mail.php';
	require_once 'Zend/Mail/Transport/Smtp.php';
	
	global $from_mail, $from_name, $to_mail, $to_name, $mail_server, $smtp_server, $smtp_port;
    
	$tr = new Zend_Mail_Transport_Smtp($smtp_server, array(
	    'port' => $smtp_port
	));

	Zend_Mail::setDefaultTransport($tr);
	$mail = new Zend_Mail('UTF-8');
	$mail->setBodyHtml($bodytext);
	$mail->setFrom($from_mail, $from_name);
	$mail->addTo($to_mail, $to_name);
	$mail->setSubject($subject);
	$mail->send();
}

function saveRecordToFile($line) {

	$fh = fopen('bugreports.txt', 'a+');

   	fwrite($fh, $line . "\n");

   	fclose($fh);

}

function getMsgFromHTML($_html) {
    $matches = array();
    if (preg_match("/(Notice|Error|.*error|Warning)(<\/b>:|:).*/", $_html, $matches)) {
        $msg = strip_tags($matches[0]);
        return $msg;
    }
    
    if (preg_match("/<!DOCTYPE/", $_html)) {
        if (preg_match_all("/<script/", $_html, $matches) > 10) {
            return 'index.php (development mode)';
        }
        return 'index.php (release mode)';
    }
    
    return 'unknown error';
}

function getInstallationByTinebaseVersionId($id) {
	global $tinebase_version_id_to_installation;
	
	if (isset($tinebase_version_id_to_installation[$id])) {
	   return $tinebase_version_id_to_installation[$id];
	}
	
	return null;
}
