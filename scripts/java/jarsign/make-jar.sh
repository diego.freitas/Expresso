#!/bin/bash
cd `dirname $0`

runId=`eval date +%y%m%d_%k%M%S`

JAVA_HOME=/opt/jdk
TARGET_JRE=/opt/jre1.6.0_45
DSANAME="EXPRESSO"
ALIAS="SERVICO FEDERAL DE PROCESSAMENTO DE DADOS SERPRO :CEAGO's Autoridade Certificadora do SERPRO Final v3 ID"

echo "Digite a senha da keystore"
read -s PASSWD

DOMAINS=""
while read line
do
    if [[ ${line:0:1} != '#' ]] && [[ ! -z ${line} ]]
    then
        DOMAINS+=" $line"
    fi
done < domains.config

DOMAINS=$(echo "$DOMAINS" | sed -e 's/^ //' -e 's/ / \n /g')
echo "Domains: $DOMAINS"

if [ ! -d "out" ]; then
    mkdir "out"
fi

mkdir "out/$runId"
mkdir "out/$runId/proguard"
mkdir "out/$runId/dist"

"$JAVA_HOME/bin/java" -jar proguard.jar @expressoV3config.pro \
    -libraryjars \
        "$TARGET_JRE/lib/rt.jar":\
        "$TARGET_JRE/lib/plugin.jar":\
        "$TARGET_JRE/lib/javaws.jar":\
        "$TARGET_JRE/lib/jce.jar":\
        "$TARGET_JRE/lib/jsse.jar":\
        "$TARGET_JRE/lib/charsets.jar":\
        "$TARGET_JRE/lib/ext/dnsns.jar":\
        "$TARGET_JRE/lib/ext/localedata.jar":\
        "$TARGET_JRE/lib/ext/sunjce_provider.jar":\
        "$TARGET_JRE/lib/ext/sunpkcs11.jar":\
        "$TARGET_JRE/lib/security/US_export_policy.jar":\
        "$TARGET_JRE/lib/security/local_policy.jar" \
    -outjars "out/$runId/proguard/ExpressoCertMail-all.jar" \
    -printseeds "out/$runId/proguard/proguard_seeds.txt" \
    -printusage "out/$runId/proguard/proguard_usage.txt" \
    -printconfiguration "out/$runId/proguard/proguard_config.txt"

cp "out/$runId/proguard/ExpressoCertMail-all.jar" "out/$runId/dist/ExpressoCertMail-all.jar"

"$JAVA_HOME/bin/jar" ufv "out/$runId/dist/ExpressoCertMail-all.jar" \
    META-INF/THIRD-PARTY/MIME4J-CORE \
    META-INF/THIRD-PARTY/MIME4J-DOM \
    META-INF/THIRD-PARTY/BCMAIL \
    META-INF/THIRD-PARTY/BCPKIX \
    META-INF/THIRD-PARTY/BCPROV \
    META-INF/THIRD-PARTY/COMMONS-CODEC \
    META-INF/THIRD-PARTY/COMMONS-LANG3 \
    META-INF/THIRD-PARTY/COMMONS-LOGGING \
    META-INF/THIRD-PARTY/GUAVA \
    META-INF/THIRD-PARTY/HTTP-CLIENT \
    META-INF/THIRD-PARTY/HTTP-CORE \
    META-INF/THIRD-PARTY/JACKSON-CORE \
    META-INF/THIRD-PARTY/JACKSON-MAPPER \
    META-INF/THIRD-PARTY/JAVA-MAIL \
    META-INF/THIRD-PARTY/JERICHO-HTML \
    META-INF/THIRD-PARTY/PICOCONTAINER

sleep 3

touch manifest.mf
echo "Application-Name: ExpressoMail Security" >> manifest.mf
echo "Permissions: all-permissions" >> manifest.mf
echo "Codebase: $DOMAINS" >> manifest.mf
echo "Caller-Allowable-Codebase: $DOMAINS" >> manifest.mf
echo "" >> manifest.mf

"$JAVA_HOME/bin/jar" -uvmf manifest.mf "out/$runId/dist/ExpressoCertMail-all.jar"

sleep 3

rm manifest.mf

echo $PASSWD | "$JAVA_HOME/bin/jarsigner" \
    -keystore NONE \
    -storetype PKCS11 \
    -providerClass sun.security.pkcs11.SunPKCS11 \
    -providerArg token.config \
    -sigfile $DSANAME \
    -verbose \
    "out/$runId/dist/ExpressoCertMail-all.jar" \
    "$ALIAS"

sleep 3
