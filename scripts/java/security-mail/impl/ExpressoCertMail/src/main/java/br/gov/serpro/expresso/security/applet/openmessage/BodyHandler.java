package br.gov.serpro.expresso.security.applet.openmessage;

import java.util.Deque;
import java.util.logging.Logger;
import org.apache.james.mime4j.dom.BinaryBody;
import org.apache.james.mime4j.dom.Body;
import org.apache.james.mime4j.dom.Message;
import org.apache.james.mime4j.dom.Multipart;
import org.apache.james.mime4j.dom.SingleBody;
import org.apache.james.mime4j.dom.TextBody;

public abstract class BodyHandler {

    private static final Logger logger = Logger.getLogger(BodyHandler.class.getName());

    private boolean quitted;

    public final boolean isQuitted() {
        return quitted;
    }

    protected final void quit() {
        quitted = true;
    }

    public void body(Body body, Deque<Integer> level) {}

    public void singleBody(SingleBody singleBody, Deque<Integer> level) {}
    public void textBody(TextBody textBody, Deque<Integer> level) {}
    public void binaryBody(BinaryBody binaryBody, Deque<Integer> level) {}

    public void inlineBody(SingleBody singleBody, Deque<Integer> level) {}
    public void attachmentBody(SingleBody singleBody, Deque<Integer> level) {}

    public void multipart(Multipart multipart, Deque<Integer> level) {}
    public void endMultipart(Multipart multipart, Deque<Integer> level) {}

    //embedded message (mimetype message/rfc822)
    //is both an entity and a body
    public void embedded(Message message, Deque<Integer> level) {}
    public void endEmbedded(Message message, Deque<Integer> level) {}


}
