<?php
/**
 * Tine 2.0
 *
 * @package     Addressbook
 * @subpackage  Frontend
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Lars Kneschke <l.kneschke@metaways.de>
 * @copyright   Copyright (c) 2007-2012 Metaways Infosystems GmbH (http://www.metaways.de)
 */

/**
 * Addressbook_Frontend_Json
 *
 * This class handles all Json requests for the addressbook application
 *
 * @package     Addressbook
 * @subpackage  Frontend
 */
class Addressbook_Frontend_Json extends Tinebase_Frontend_Json_Abstract
{
    /**
     * app name
     * 
     * @var string
     */
    protected $_applicationName = 'Addressbook';
    
    /**
     * resolve images
     * @param Tinebase_Record_RecordSet $_records
     */
    public static function resolveImages(Tinebase_Record_RecordSet $_records)
    {
        foreach($_records as &$record) {
            if($record['jpegphoto'] == '1') {
                $record['jpegphoto'] = Tinebase_Model_Image::getImageUrl('Addressbook', $record->__get('id'), '');
            }
        }
    }

    /**
     * get one contact identified by $id
     *
     * @param string $id
     * @return array
     */
    public function getContact($id)
    {
        $record = $this->_get($id, Addressbook_Controller_Contact::getInstance());
        if($record['jpegphoto'] == '1') {
            $record['jpegphoto'] = Tinebase_Model_Image::getImageUrl('Addressbook', $record['id'], '');
        }
        return $record;
    }
    
    /**
     * Search for Certificates matching given arguments
     * 
     * @param type $_filter 
     * @return array
     */
    public function searchCertificates($filter, $paging)
    {   
        return $this->_search($filter, $paging, Addressbook_Controller_Certificate::getInstance(), 'Addressbook_Model_CertificateFilter');
    }
    
    
    /**
     * Search for contacts matching given arguments
     *
     * @param  array $filter
     * @param  array $paging
     * @return array
     */
    public function searchContacts($filter, $paging)
    {
        return $this->_search($filter, $paging, Addressbook_Controller_Contact::getInstance(), 'Addressbook_Model_ContactFilter');
    }   

    /**
     * Search for Email Addresses with the Email Model in Lists and Contacts
     *
     * @param  array $filter
     * @param  array $paging
     * @return array
     *
     * Record Proxy Pluralizer doesnt correctly pluralize EmailAddress, thus EmailAddresss is used
     *
     */
    public function searchEmailAddresss($filter, $paging)
    {
        $results = array();
        $contacts = $this->_search($filter, $paging, Addressbook_Controller_Contact::getInstance(), 'Addressbook_Model_ContactFilter');
        foreach ($contacts["results"] as $contact) {
            array_push($results, array("n_fileas" => $contact["n_fileas"],"n_fn" => $contact["n_fn"],"org_unit" => $contact["org_unit"], "email" => $contact["email"], "email" => $contact["email"], "email_home" => $contact["email_home"]));
        }

        $dont_add = false;
        if (isset($paging["start"])) {
            $paging["start"] = $paging["start"] - $contacts["totalcount"] + count($results);
            $paging["limit"] = $paging["limit"] - count($results);
            if (($paging["limit"] <= 0) || ($paging["start"] < 0)) {
                $dont_add = true;
                $paging["limit"] = 1;
                $paging["start"] = 0;
            }
        }
        if (!$dont_add) {
            $lists = $this->_search($filter, $paging, Addressbook_Controller_List::getInstance(), 'Addressbook_Model_ListFilter');
            foreach ($lists["results"] as $list) {
                $listContacts = Addressbook_Controller_List::getInstance()->searchListContactsFormated($list['id']);
                if (!(empty($listContacts[0]))){
                    array_push($results, array("n_fn" => $list["name"], "emails" => $listContacts[0]));
                }
            }
         }
         $totalcount = isset($lists["totalcount"])?$lists["totalcount"]:0 + $contacts["totalcount"];
        return array("results" => $results, "totalcount" => $totalcount);
    } 
    
    /**
     * return autocomplete suggestions for a given property and value
     * 
     * @todo have spechial controller/backend fns for this
     * @todo move to abstract json class and have tests
     *
     * @param  string   $property
     * @param  string   $startswith
     * @return array
     */
    public function autoCompleteContactProperty($property, $startswith)
    {
        if (preg_match('/[^A-Za-z0-9_]/', $property)) {
            // NOTE: it would be better to ask the model for property presece, but we can't atm.
            throw new Tasks_Exception_UnexpectedValue('bad property name');
        }
        
        $filter = new Addressbook_Model_ContactFilter(array(
            array('field' => $property, 'operator' => 'startswith', 'value' => $startswith),
        ));
        
        $paging = new Tinebase_Model_Pagination(array('sort' => $property));
        
        $values = array_unique(Addressbook_Controller_Contact::getInstance()->search($filter, $paging)->{$property});
        
        $result = array(
            'results'   => array(),
            'totalcount' => count($values)
        );
        
        foreach($values as $value) {
            $result['results'][] = array($property => $value);
        }
        
        return $result;
    }
    
    /**
     * get one list identified by $id
     *
     * @param string $id
     * @return array
     */
    public function getList($id)
    {
        return $this->_get($id, Addressbook_Controller_List::getInstance());
    }

    /**
     * save one list
     *
     * if $recordData['id'] is empty the list gets added, otherwise it gets updated
     *
     * @param  array $recordData an array of list properties
     * @param  boolean $duplicateCheck
     * @return array
     */
    public function saveList($recordData, $duplicateCheck = FALSE)
    {
        $duplicateCheck = FALSE;
        return $this->_save($recordData, Addressbook_Controller_List::getInstance(), 'List', 'id', array($duplicateCheck));
    }

    /**
     * Search for lists matching given arguments
     *
     * @param  array $filter
     * @param  array $paging
     * @return array
     */
    public function searchLists($filter, $paging)
    {
        return $this->_search($filter, $paging, Addressbook_Controller_List::getInstance(), 'Addressbook_Model_ListFilter');
    }

    /**
     * Return a formated to use list of emails name<email>
     * @param string|array $listId
     * @return array
     */
    public function searchListContactsFormated($listId)
    {
        return Addressbook_Controller_List::getInstance()->searchListContactsFormated($listId);
    }

    /**
     * delete multiple lists
     *
     * @param array $ids list of listId's to delete
     * @return array
     */
    public function deleteLists($ids)
    {
        return $this->_delete($ids, Addressbook_Controller_List::getInstance());
    }

    /*
     * Search for the contacts of a list matching given arguments
     *
     * @param  array $filter
     * @param  array $paging
     * @return array
     *
     */
    public function searchListContacts($filter, $paging)
    {
        return Addressbook_Controller_List::getInstance()->searchListContacts($filter, $paging);
    }

    /**
     * delete multiple contacts
     *
     * @param array $ids list of contactId's to delete
     * @return array
     */
    public function deleteContacts($ids)
    {
        return $this->_delete($ids, Addressbook_Controller_Contact::getInstance());
    }
    
    /**
     * save one contact
     *
     * if $recordData['id'] is empty the contact gets added, otherwise it gets updated
     *
     * @param  array $recordData an array of contact properties
     * @param  boolean $duplicateCheck
     * @return array
     */
    public function saveContact($recordData, $duplicateCheck = TRUE)
    {
        return $this->_save($recordData, Addressbook_Controller_Contact::getInstance(), 'Contact', 'id', array($duplicateCheck));
    }
    
    /**
     * import contacts
     * 
     * @param string $tempFileId to import
     * @param string $definitionId
     * @param array $importOptions
     * @param array $clientRecordData
     * @return array
     */
    public function importContacts($tempFileId, $definitionId, $importOptions, $clientRecordData = array())
    {
        return $this->_import($tempFileId, $definitionId, $importOptions, $clientRecordData);
    }
    
    /**
    * get contact information from string by parsing it using predefined rules
    *
    * @param string $address
    * @return array
    */
    public function parseAddressData($address)
    {
        $result = Addressbook_Controller_Contact::getInstance()->parseAddressData($address);
        $contactData = $this->_recordToJson($result['contact']);
        
        unset($contactData['jpegphoto']);
        unset($contactData['salutation']);
        
        return array(
            'contact'             => $contactData,
            'unrecognizedTokens'  => $result['unrecognizedTokens'],
        );
    }
    
    /**
     * get default addressbook
     * 
     * @return array
     */
    public function getDefaultAddressbook()
    {
        $defaultAddressbook = Addressbook_Controller_Contact::getInstance()->getDefaultAddressbook();
        $defaultAddressbookArray = $defaultAddressbook->toArray();
        $defaultAddressbookArray['account_grants'] = Tinebase_Container::getInstance()->getGrantsOfAccount(Tinebase_Core::getUser(), $defaultAddressbook->getId())->toArray();
        
        return $defaultAddressbookArray;
    }
    
    /**
    * returns contact prepared for json transport
    *
    * @param Addressbook_Model_Contact $_contact
    * @return array contact data
    */
    protected function _recordToJson($_contact)
    {
        $result = parent::_recordToJson($_contact);
        $result['jpegphoto'] = $this->_getImageLink($result);
    
        return $result;
    }
    
    /**
     * returns multiple records prepared for json transport
     *
     * @param Tinebase_Record_RecordSet $_records Tinebase_Record_Abstract
     * @param Tinebase_Model_Filter_FilterGroup
     * @param Tinebase_Model_Pagination $_pagination
     * @return array data
     */
    protected function _multipleRecordsToJson(Tinebase_Record_RecordSet $_records, $_filter = NULL, $_pagination = NULL)
    {
        $result = parent::_multipleRecordsToJson($_records, $_filter, $_pagination);
        
        foreach ($result as &$contact) {
            $contact['jpegphoto'] = $this->_getImageLink($contact);
        }
        
        return $result;
    }

    /**
     * returns a image link
     * 
     * @param  array $contactArray
     * @return string
     */
    protected function _getImageLink($contactArray)
    {
        $link = 'images/empty_photo_blank.png';
        if (! empty($contactArray['jpegphoto'])) {
            $link = Tinebase_Model_Image::getImageUrl('Addressbook', $contactArray['id'], '');
        } else if (isset($contactArray['salutation']) && ! empty($contactArray['salutation'])) {
            $salutations = Addressbook_Config::getInstance()->get(Addressbook_Config::CONTACT_SALUTATION, NULL);
            if ($salutations && $salutations->records instanceof Tinebase_Record_RecordSet) {
                $salutationRecord = $salutations->records->getById($contactArray['salutation']);
                if ($salutationRecord && $salutationRecord->image) {
                    $link = $salutationRecord->image;
                }
            }
        }
        
        return $link;
    }

    /**
     * Returns registry data of addressbook.
     * @see Tinebase_Application_Json_Abstract
     * 
     * @return mixed array 'variable name' => 'data'
     */
    public function getRegistryData()
    {
        $registryData = array(
            'defaultAddressbook'        => $this->getDefaultAddressbook(),
        );
        return $registryData;
    }

    /**
     * Return import definitions data (lazy loading)
     */
    public function getImportDefinitionsData()
    {
        $definitionConverter = new Tinebase_Convert_ImportExportDefinition_Json();
        $importDefinitions = $this->_getImportDefinitions();
        $defaultDefinition = $this->_getDefaultImportDefinition($importDefinitions);

        $definitionsData = array(
            'defaultImportDefinition'   => $definitionConverter->fromTine20Model($defaultDefinition),
            'importDefinitions'         => array(
                'results'               => $definitionConverter->fromTine20RecordSet($importDefinitions),
                'totalcount'            => count($importDefinitions),
            ),
        );
        return $definitionsData;
    }
    
    /**
     * get addressbook import definitions
     * 
     * @return Tinebase_Record_RecordSet
     * 
     * @todo generalize this
     */
    protected function _getImportDefinitions()
    {
        $filter = new Tinebase_Model_ImportExportDefinitionFilter(array(
            array('field' => 'application_id',  'operator' => 'equals', 'value' => Tinebase_Application::getInstance()->getApplicationByName('Addressbook')->getId()),
            array('field' => 'type',            'operator' => 'equals', 'value' => 'import'),
        ));
        
        $importDefinitions = Tinebase_ImportExportDefinition::getInstance()->search($filter);
        
        return $importDefinitions;
    }
    
    /**
     * get default definition
     * 
     * @param Tinebase_Record_RecordSet $_importDefinitions
     * @return Tinebase_Model_ImportExportDefinition
     * 
     * @todo generalize this
     */
    protected function _getDefaultImportDefinition($_importDefinitions)
    {
        try {
            $defaultDefinition = Tinebase_ImportExportDefinition::getInstance()->getByName('adb_tine_import_csv');
        } catch (Tinebase_Exception_NotFound $tenf) {
            if (count($_importDefinitions) > 0) {
                $defaultDefinition = $_importDefinitions->getFirstRecord();
            } else {
                Tinebase_Core::getLogger()->warn(__METHOD__ . '::' . __LINE__ . ' No import definitions found for Addressbook');
                $defaultDefinition = NULL;
            }
        }
        
        return $defaultDefinition;
    }
}
