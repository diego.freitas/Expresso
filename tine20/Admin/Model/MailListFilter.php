<?php
/**
 * Tine 2.0
 *
 * @package     Admin
 * @subpackage  MailList
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Fernando Wendt <fernando-alberto.wendt@serpro.gov.br>
 * @copyright   Copyright (c) 2014 SERPRO (https://www.serpro.gov.br)
 */

/**
 *  MailList filter class
 *
 * @package     Tinebase
 * @subpackage  MailList
 */
class Admin_Model_MailListFilter extends Tinebase_Model_Filter_FilterGroup
{
    /**
     * @var string application of this filter group
     */
    protected $_applicationName = 'Admin';

    /**
     * @var string name of model this filter group is designed for
     */
    protected $_modelName = 'Admin_Model_MailList';

    /**
     * @var string class name of this filter group
     *      this is needed to overcome the static late binding
     *      limitation in php < 5.3
     */
    protected $_className = 'Admin_Model_MailListFilter';

    protected $_filterModel = array(
        'query'              => array('filter' => 'Tinebase_Model_Filter_Query', 'options' => array('fields' => array('mail', 'name', 'description'))),
        'uid'                => array('filter' => 'Tinebase_Model_Filter_Text'),
        'uidnumber'          => array('filter' => 'Tinebase_Model_Filter_Text'),
        'name'               => array('filter' => 'Tinebase_Model_Filter_Text'),
        'description'        => array('filter' => 'Tinebase_Model_Filter_Text'),
        'mail'               => array('filter' => 'Tinebase_Model_Filter_Text'),
        'admins'             => array('filter' => 'Tinebase_Model_Filter_Text'),
        'moderation'         => array('filter' => 'Tinebase_Model_Filter_Text'),
        'notmoderated'       => array('filter' => 'Tinebase_Model_Filter_Text'),
        'members'            => array('filter' => 'Tinebase_Model_Filter_Text'),
        'externalmembers'    => array('filter' => 'Tinebase_Model_Filter_Text'),
    );
}
