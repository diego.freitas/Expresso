/*
 * Tine 2.0
 *
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Fernando Alberto Reuter Wendt <fernando-alberto.wendt@serpro.gov.br>
 * @copyright   Copyright (c) 2014 SERPRO - Serviço Federal de Processamento de Dados (https://www.serpro.gov.br)
 *
 */

Ext.ns('Tine.Admin.MailLists');

/**
 * @namespace   Tine.Admin.MailLists
 * @class       Tine.Admin.MailList.ExportDialog
 * @extends     Tine.widgets.dialog.EditRecord
 */
Tine.Admin.MailListsExportDialog = Ext.extend(Tine.widgets.dialog.ExportDialog, {
    /*
     * @private
     */
    windowNamePrefix: 'maillistExportWindow_',
    appName: 'Admin',
    recordClass: Tine.Admin.Model.MailLists,
    recordProxy: Tine.Admin.MailListsBackend,
    evalGrants: false,

    /**
     * @private
     */
    initComponent: function () {
        Tine.Admin.MailListsExportDialog.superclass.initComponent.call(this);
    },

    /**
     * returns dialog
     */
    getFormItems: function () {
        this.displayFieldStyle = {
            border: 'silver 1px solid',
            padding: '3px',
            height: '11px'
        };

        this.definitionsStore = Ext.create('Ext.data.Store', {
        fields: ['export_format'],
        data: [{
            "abbr": "CSV",
            "name": "CSV"
        }, {
            "abbr": "XML",
            "name": "XML"
        }, {
            "abbr": "TXT",
            "name": "Text"
        }
        ]
    });

        var editGroupDialog = {
            id: 'tabpanelMailList',
            border: false,
            plain: true,
            activeTab: 0,
            defaults: {
                border: false,
                frame: true,
                layoutConfig: {type: 'fit', align: 'stretch', pack: 'start'}
            },
            items: [{
                xtype: 'combo',
                fieldLabel: _('Export definition'),
                name:'export_definition_id',
                store: this.definitionsStore,
                displayField:'label',
                mode: 'local',
                triggerAction: 'all',
                editable: false,
                allowBlank: false,
                forceSelection: true,
                emptyText: _('Select Export Definition ...'),
                valueField: 'id',
                value: (this.definitionsStore.getCount() > 0) ? this.definitionsStore.getAt(0).id : null
            }
            ]
        };

        return editGroupDialog;

    }
});


/**
 * Mail List Export Popup
 */
Tine.Admin.MailListsExportDialog.openWindow = function (config) {
    var id = (config.record && config.record.id) ? config.record.id : 0;
    var window = Tine.WindowFactory.getWindow({
        width: 300,
        height: 200,
        name: Tine.Admin.MailListsExportDialog.prototype.windowNamePrefix + id,
        contentPanelConstructor: 'Tine.Admin.MailListsExportDialog',
        contentPanelConstructorConfig: config
    });
    return window;
};