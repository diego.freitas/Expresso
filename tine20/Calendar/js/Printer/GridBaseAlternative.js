Ext.ns('Tine.Calendar.Printer');


/**
 * @class   Tine.Calendar.Printer.BaseRenderer
 * @extends Ext.ux.Printer.BaseRenderer
 * 
 * Printig renderer for Ext.ux.printing
 */
Tine.Calendar.Printer.GridBaseAlternativeRenderer = Ext.extend(Ext.ux.Printer.BaseRenderer, {
    stylesheetPath: 'Calendar/css/print.css',
    
    extraTitle: '',
    titleStyle: '',
//    generateBody: function(view) {
//        var days = [];
//        
//        // iterate days
//        for (var dayStart, dayEnd, dayEvents, i=0; i<view.numOfDays; i++) {
//            dayStart = view.startDate.add(Date.DAY, i);
//            dayEnd   = dayStart.add(Date.DAY, 1).add(Date.SECOND, -1);
//            
//            // get events in this day
//            dayEvents = view.store.queryBy(function(event){
//                return event.data.dtstart.getTime() < dayEnd.getTime() && event.data.dtend.getTime() > dayStart.getTime();
//            });
//            
//            days.push(this.generateDay(dayStart, dayEnd, dayEvents));
//        }
//        
//        var topic = this.generateHeader(view);
//        var body  = 
//        return view.numOfDays === 1 ? days[0] : String.format('<table>{0}</table>', this.generateCalRows(days, view.numOfDays < 9 ? 2 : 7));
//    },
    
    generateTitle: function(view) {
        var container = view.grid.app.mainScreen.WestPanel.TreePanel.lastClickedNode;
        var userFullName = container && container.attributes.owner ? container.attributes.owner.accountFullName : Tine.Tinebase.registry.get('currentAccount').accountDisplayName;
        return ['<table class="mini-cal-header-table" cellspacing="0"><tr><th class="cal-print-title">', this.extraTitle,  this.getTitle(view), '<br><br><br><span style="font-size: 10px;">', userFullName,'</span></th>',this.generateMiniCal(view),'</tr></table>'].join('');

    },

    generateMiniCal: function(view){
        this.app = Tine.Tinebase.appMgr.get('Calendar');
        var miniCalNum = this.app.getRegistry().get('preferences').get('minicalprint');
        if(miniCalNum == 1){
            return String.format('<th class ="cal-print-minical-second">{0}</th>',this.getMiniCal(0,view.startDate));;
        }
        else if(miniCalNum == 2){
            return String.format('<th class ="cal-print-minical-first">{0}</th><th class ="cal-print-minical-second">{1}</th>',this.getMiniCal(0,view.startDate),this.getMiniCal(1,view.startDate));

        }
        else if(miniCalNum == 3){
            return String.format('<th class ="cal-print-minical-first">{0}</th><th class ="cal-print-minical-first">{1}</th><th class ="cal-print-minical-second">{2}</th>',this.getMiniCal(-1,view.startDate),this.getMiniCal(0,view.startDate),this.getMiniCal(1,view.startDate));
        }
    },

    /*
    * get minical calendar
    *  month is the numder of month, 0 is current month
    *
    */
    getMiniCal: function(month,date){
        var firstDay = new Date(date.getFullYear(), date.getMonth()+ month, 1);
        var lastDay = new Date(date.getFullYear(), date.getMonth() + month +1, 0);
        monthName = String.format('<td align="center" class="mini-date-title" id="firstMonthName">{0}</td>', firstDay.format('F Y'));
        tBody = '<tr>';
        for (j = 0 ; j < firstDay.getDay(); j++){
            tBody += '<td class="mini-date-inner"></td>';
        }
        var currentDate = firstDay;
        for (i = firstDay.getDate(); i <= lastDay.getDate();i++){
            if(currentDate.getDay()%7 == 0){
                tBody += '</tr><tr border-collapse="collapse">';
            }
            tBody += String.format('<td class="mini-date-inner"><span>{0}</span></td>',i)
            currentDate = currentDate.add(Date.DAY, 1);
        }
        tBody += '</tr>';
        minical = String.format('<table cellspacing="0" class="mini-cal-table" id="firstmini" align="right"><tbody><tr>{0}</tr><tr><td colspan="3" class="mini-date-inner"><table cellspacing="0" class="mini-date-inner"><thead><tr><th class="mini-cal-span"><span>D</span></th><th class="mini-cal-span"><span>S</span></th><th class="mini-cal-span"><span>T</span></th><th class="mini-cal-span"><span>Q</span></th><th class="mini-cal-span"><span>Q</span></th><th class="mini-cal-span"><span>S</span></th><th class="mini-cal-span"><span>S</span></th></tr></thead><tbody class="mini-cal-span">{1}</tbody></table></td></tr></tbody></table>',monthName,tBody);
        return minical;
    },
    
    generateCalRows: function(days, numCols, alignHorizontal) {
        var row, col, cellsHtml, idx,
            numRows = Math.ceil(days.length/numCols),
            rowsHtml = '';
        
        for (row=0; row<numRows; row++) {
            cellsHtml = '';
            //offset = row*numCols;
            
            for (col=0; col<numCols; col++) {
                idx = alignHorizontal ? row*numCols + col: col*numRows + row;
                cellsHtml += String.format('<td class="cal-print-daycell" style="vertical-align: top;">{0}</td>', days[idx] || '');
            }
            
            rowsHtml += String.format('<tr class="cal-print-dayrow" style="height: {1}mm">{0}</tr>', cellsHtml, this.paperHeight/numRows);
        }
        
        return rowsHtml;
    },
    
    generateDay: function(dayStart, dayEnd, dayEvents, cols, numOfDays) {
        var dayBody = '',
        hrCurrentEvent,
        hrNextEvent;
        
        for (var n = 0; n < dayEvents.items.length; n++) {
            
            var start = dayEvents.items[n].data.dtstart.getTime() <= dayStart.getTime() ? dayStart : dayEvents.items[n].data.dtstart,
                end   = dayEvents.items[n].data.dtend.getTime() > dayEnd.getTime() ? dayEnd : dayEvents.items[n].data.dtend;

            //writes each event on its own line
            dayBody += this.eventTpl.apply({
                startTime: (cols.indexOf('dtstart') !== -1) ? ( dayEvents.items[n].data.is_all_day_event ? '' : start.format('H:i') ) : '',
                untilseperator: dayEvents.items[n].data.is_all_day_event ? '' : '-',
                endTime: (cols.indexOf('dtend') !== -1) ? ( dayEvents.items[n].data.is_all_day_event ? '' : end.format('H:i') ) : '',
                summary: (cols.indexOf('summary') !== -1) ? ( Ext.util.Format.htmlEncode(dayEvents.items[n].data.summary) ) : '',
                duration: dayEvents.items[n].data.is_all_day_event ? '('+Tine.Tinebase.appMgr.get('Calendar').i18n._('whole day')+')' : 
                    '('+Tine.Tinebase.common.minutesRenderer(Math.round((end.getTime() - start.getTime())/(1000*60)), '{0}:{1}', 'i')+')',
                description: (cols.indexOf('description') !== -1) ? dayEvents.items[n].data.description : '',
                attendees: this.getAttendees(dayEvents.items[n]),
                location: (cols.indexOf('location') !== -1) ? ( dayEvents.items[n].data.location ? Tine.Tinebase.appMgr.get('Calendar').i18n._('Location') + ': ' + dayEvents.items[n].data.location : '' ) : ''
            });
            
            if (numOfDays == 1 && dayEvents.items[n+1]) {
                
                hrCurrentEvent = dayEvents.items[n].data.dtend.getHours();
                hrNextEvent = dayEvents.items[n+1].data.dtstart.getHours();
                
                if ( dayEvents.items[n].data.dtend.getMinutes() > 0 ) {
                    hrCurrentEvent = hrCurrentEvent + 1;
                }
                
                //while there is free time between the events, it will write a
                //blank line
                while (hrNextEvent > hrCurrentEvent) {

                    dayBody += this.eventTpl.apply({
                        startTime: hrCurrentEvent +':00',
                        untilseperator: '-',
                        endTime: hrCurrentEvent+1 +':00',
                        summary: '',
                        duration: '',
                        description: '',
                        location: ''
                    });

                    hrCurrentEvent = hrCurrentEvent + 1;
                }
            }
        }
        
        var dayHeader = this.dayTpl.apply({
            dayOfMonth: dayStart.format('j'),
            weekDay: dayStart.format('l')
        });
        return String.format('<table class="cal-print-daysview-day"><tr>{0}</tr>{1}</table>', dayHeader, dayBody);
    },

    getAttendees: function(event){
        var attendees ="";
        Ext.each(event.data.attendee, function(attender) {
            attendees += attender.user_id.n_fn + ", ";
        }, this);

        return attendees.substring(0,attendees.length -2 );
    },

    
    splitDays: function(ds, startDate, numOfDays, cm, returnData) {
        var days = [];
        
        //columns that will be sent for printing, according to selection
        var cols = [];
        for (var x = 0; x < cm.columns.length; x++) {
            if (! (cm.columns[x].hidden && (cm.columns[x].hidden = true)) ) {
                cols.push(cm.columns[x].id);
            }
        }
        
        // iterate days
        for (var dayStart, dayEnd, dayEvents, i=0; i<numOfDays; i++) {
            dayStart = startDate.add(Date.DAY, i);
            dayEnd   = dayStart.add(Date.DAY, 1).add(Date.SECOND, -1);
            
            // get events in this day
            dayEvents = ds.queryBy(function(event){
                return event.data.dtstart.getTime() < dayEnd.getTime() && event.data.dtend.getTime() > dayStart.getTime();
            });
            
            days.push(returnData ? {
                dayStart: dayStart,
                dayEnd: dayEnd,
                dayEvents: dayEvents
            } : this.generateDay(dayStart, dayEnd, dayEvents, cols, numOfDays));
        }
        
        return days;
    },
    
    dayTpl: new Ext.XTemplate(
        '<tr>',
            '<th  colspan="5">',
                '<span class="cal-print-gridview-dayOfMonth">{dayOfMonth}</span>',
                '<span class="cal-print-gridview-weekDay">{weekDay}</span>',
            '</th>', 
        '</tr>'
    ),
    
    /**
     * @property eventTpl
     * @type Ext.XTemplate
     * The XTemplate used to create the headings row. By default this just uses <th> elements, override to provide your own
     */
    eventTpl: new Ext.XTemplate(
        '<tr>',
            '<td class="cal-print-daysview-day-color"><span style="color: {color};">&#9673;</span></td>',
            '<td class="cal-print-gridview-starttime">{startTime}</td>',
            '<td class="cal-print-gridview-untilseperator">{untilseperator}</td>',
            '<td class="cal-print-gridview-endtime">{endTime}</td>',
            '<td class="cal-print-gridview-summary"><span class="cal-print-gridview-summary-font">{summary}</span> <span class="cal-print-daysview-day-duration">{duration}</span></td>',
        '</tr>',
        '<tr>',
             '<td colspan="5" class="cal-print-daysview-day-description">{location}</td>',
        '</tr>',
         '<tr>',
             '<td colspan="5" class="cal-print-daysview-day-description">{description}</td>',
        '</tr>',
         '<tr>',
             '<td colspan="5" class="cal-print-daysview-day-attendees">{attendees}</td>',
        '</tr>'
    )
    
});
