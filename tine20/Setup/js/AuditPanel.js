/*
 * Tine 2.0
 *
 * @package     Setup
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Lucas Alberto Santos <lucas-alberto.santos@serpro.gov.br>
 * @copyright   Copyright (c) 2014 Serpro (http://www.serpro.gov.br)
 *
 */

/*global Ext, Tine*/

Ext.ns('Tine', 'Tine.Setup');

/**
 * Setup Authentication Manager
 *
 * @namespace   Tine.Setup
 * @class       Tine.Setup.AuditPanel
 * @extends     Tine.Tinebase.widgets.form.ConfigPanel
 *
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Lucas Alberto Santos <lucas-alberto.santos@serpro.gov.br>
 * @copyright   Copyright (c) 2014 Serpro (http://www.serpro.gov.br)
 *
 * @param       {Object} config
 * @constructor
 * Create a new Tine.Setup.AuditPanel
 */
Tine.Setup.AuditPanel = Ext.extend(Tine.Tinebase.widgets.form.ConfigPanel, {

    /**
     * @property idPrefix DOM Id prefix
     * @type String
     */
    idPrefix: null,

    /**
     * @private
     * panel cfg
     */
    saveMethod: 'Setup.saveConfig',
    registryKey: 'configData',

    /**
     * @private
     * field index counter
     */
    tabIndexCounter: 1,

    /**
     * @private
     */
    initComponent: function () {
        this.idPrefix                              = Ext.id();
        Tine.Setup.AuthenticationPanel.superclass.initComponent.call(this);
        this.action_saveConfig.setDisabled(false);
    },

    /**
     * @private
     */
    onRender: function (ct, position) {
        Tine.Setup.AuditPanel.superclass.onRender.call(this, ct, position);
    },

    /**
     * transforms form data into a config object
     *
     * @hack   smuggle termsAccept in
     * @return {Object} configData
     */
    form2config: function () {
        var configData = this.supr().form2config.call(this);
        return configData;
    },

    /**
     * get tab index for field
     *
     * @return {Integer}
     */
    getTabIndex: function () {
        return this.tabIndexCounter++;
    },

   /**
     * returns config manager form
     *
     * @private
     * @return {Array} items
     */
    getFormItems: function () {
        var setupRequired = Tine.Setup.registry.get('setupRequired');

        // common config for all combos in this setup
        var commonComboConfig = {
            xtype: 'combo',
            listWidth: 300,
            mode: 'local',
            forceSelection: true,
            allowEmpty: false,
            triggerAction: 'all',
            editable: false,
            tabIndex: this.getTabIndex
        };

        return [{
            xtype: 'fieldset',
            collapsible: false,
            autoHeight: true,
            title: this.app.i18n._('Auditing Functions'),
            items: [
                Ext.applyIf({
                    name: 'auditLogActive',
                    fieldLabel: this.app.i18n._('Enable audit log for module Admin'),
                    value: Tine.Setup.registry.get(this.registryKey).auditLogActive,
                    store: [[false, this.app.i18n._('No')], [true,this.app.i18n._('Yes')]]
                }, commonComboConfig)
            ]
        }];
    },

    /**
     * applies registry state to this cmp
     */
    applyRegistryState: function () {
    },

    /**
     * checks if form is valid
     * - password fields are equal
     *
     * @return {Boolean}
     */
    isValid: function () {
        var form = this.getForm();
        var result = form.isValid();
        return result;
    },

    /**
     * @private
     */
    initActions: function () {
        this.action_downloadConfig = new Ext.Action({
            text: this.app.i18n._('Download config file'),
            iconCls: 'setup_action_download_config',
            scope: this,
            handler: this.onDownloadConfig
        });

        this.actionToolbarItems = [this.action_downloadConfig];

        Tine.Setup.ConfigManagerPanel.superclass.initActions.apply(this, arguments);
    },

    onDownloadConfig: function() {
        if (this.isValid()) {
            var configData = this.form2config();

            var downloader = new Ext.ux.file.Download({
                url: Tine.Tinebase.tineInit.requestUrl,
                params: {
                    method: 'Setup.downloadConfig',
                    data: Ext.encode(configData)
                }
            });
            downloader.start();
        } else {
            this.alertInvalidData();
        }
    }
});