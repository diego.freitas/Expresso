#
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: ExpressoBr - Setup\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-04-09 17:23-0300\n"
"PO-Revision-Date: 2015-08-24 15:12-0300\n"
"Last-Translator: Cassiano Dal Pizzol <cassiano.dalpizzol@serpro.gov.br>\n"
"Language-Team: ExpressoBr Translators\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: _\n"
"X-Poedit-Basepath: ..\n"
"X-Generator: Poedit 1.5.4\n"
"X-Poedit-SearchPath-0: js\n"

#: js/TermsPanel.js:80
msgid "Accept Terms and Conditions"
msgstr "Aceptar Términos y Condiciones"

#: js/AuthenticationPanel.js:365 js/AuthenticationPanel.js:416
msgid "Account canonical form"
msgstr "Formulación de cuenta canónica"

#: js/AuthenticationPanel.js:371 js/AuthenticationPanel.js:422
msgid "Account domain name "
msgstr ""

#: js/AuthenticationPanel.js:374 js/AuthenticationPanel.js:425
msgid "Account domain short name"
msgstr ""

#: js/AuthenticationPanel.js:501
msgid "Accounts storage"
msgstr "Almacenamiento de cuentas"

#: js/ConfigManagerPanel.js:475
msgid "Addressbook Map panel"
msgstr ""

#: js/AuthenticationPanel.js:452 js/EmailPanel.js:212
msgid "Append domain to login name"
msgstr ""

#: js/Setup.js:82
msgid "Application Manager"
msgstr "Administrador de Aplicaciones"

#: js/EmailPanel.js:306
msgid "Authentication"
msgstr "Autentificación"

#: js/AuthenticationPanel.js:338
msgid "Authentication provider"
msgstr "Proveedor de autentificación"

#: js/Setup.js:70
msgid "Authentication/Accounts"
msgstr "Autentificación/Cuentas"

#: js/ApplicationGridPanel.js:67
msgid "Available Version"
msgstr "Versión Disponible"

#: js/AuthenticationPanel.js:266 js/AuthenticationPanel.js:279
#: js/ConfigManagerPanel.js:147 js/ConfigManagerPanel.js:159
#: js/ConfigManagerPanel.js:171 js/ConfigManagerPanel.js:183
#: js/EmailPanel.js:146
msgid "Backend"
msgstr "Administración"

#: js/AuthenticationPanel.js:409
msgid "Base DN"
msgstr "Base DN"

#: js/AuthenticationPanel.js:403 js/AuthenticationPanel.js:552
msgid "Bind requires DN"
msgstr "Bind requiere DN"

#: js/ConfigManagerPanel.js:489
msgid "Bugs Report"
msgstr ""

#: js/ConfigManagerPanel.js:270
msgid "Caching"
msgstr "Cacheo"

#: js/EnvCheckGridPanel.js:160
msgid "Check"
msgstr "Verificar"

#: js/AuthenticationPanel.js:292
msgid "Check for expired password"
msgstr ""

#: js/Setup.js:64
msgid "Config Manager"
msgstr "Administrador de Configuración"

#: js/ConfigManagerPanel.js:542
msgid "Config file is not writable"
msgstr "El archivo de configuración no tiene permisos de escritura"

#: js/EmailPanel.js:258
msgid "Cyrus Admin"
msgstr ""

#: js/EmailPanel.js:261
msgid "Cyrus Admin Password"
msgstr ""

#: js/ConfigManagerPanel.js:219 js/ConfigManagerPanel.js:237
msgid "Database"
msgstr "Base de Datos"

#: js/AuthenticationPanel.js:527 js/AuthenticationPanel.js:617
msgid "Default admin group name"
msgstr "Nombre predeterminado del grupo de administradores"

#: js/AuthenticationPanel.js:523 js/AuthenticationPanel.js:614
msgid "Default user group name"
msgstr "Nombre predeterminado del grupo de usuarios"

#: js/AuthenticationPanel.js:136
msgid "Delete all existing users and groups"
msgstr "Borrar todos los usuarios y grupos existentes"

#: js/ApplicationGridPanel.js:272
msgid "Dependency Violation"
msgstr "Violación de Dependencia"

#: js/ApplicationGridPanel.js:69
msgid "Depends on"
msgstr "Depende de"

#: js/AuthenticationPanel.js:156
msgid ""
"Disabling this feature authentication mechanism becomes more vulnerable. Are "
"you sure you want to do this?"
msgstr ""

#: js/ApplicationGridPanel.js:180
msgid "Do you really want to uninstall the application(s)?"
msgstr "¿Realmente desea desinstalar la(s) aplicación(es)?"

#: js/ConfigManagerPanel.js:562
msgid "Download config file"
msgstr "Descargando archivo de configuración"

#: js/ConfigManagerPanel.js:523
msgid "EMAIL"
msgstr ""

#: js/Setup.js:76
msgid "Email"
msgstr "Email"

#: js/AuthenticationPanel.js:751
msgid "Enable password policy"
msgstr ""

#: js/ApplicationGridPanel.js:64
msgid "Enabled"
msgstr "Activado"

#: js/EnvCheckGridPanel.js:162
msgid "Error"
msgstr "Error"

#: js/ConfigManagerPanel.js:149 js/ConfigManagerPanel.js:161
msgid "File"
msgstr ""

#: js/ConfigManagerPanel.js:263
msgid "Filename"
msgstr "Nombre de archivo"

#: js/ConfigManagerPanel.js:470
msgid "Filestore Path"
msgstr ""

#: js/ConfigManagerPanel.js:462
msgid "Filestore directory"
msgstr ""

#: js/AuthenticationPanel.js:778
msgid "Forbid part of username in password"
msgstr ""

#: js/ApplicationGridPanel.js:105
msgid "Go to {0} login"
msgstr "Ir al {0} de ingreso"

#: js/AuthenticationPanel.js:574
msgid "Group Filter"
msgstr ""

#: js/AuthenticationPanel.js:578
msgid "Group Search Scope"
msgstr ""

#: js/AuthenticationPanel.js:608
msgid "Group UUID Attribute name"
msgstr "Nombre del atributo del grupo UUID"

#: js/EmailPanel.js:479
msgid "Group or GID"
msgstr ""

#: js/AuthenticationPanel.js:571
msgid "Groups DN"
msgstr "Grupos DN"

#: js/ConfigManagerPanel.js:501
msgid "Help and documentation"
msgstr ""

#: js/EmailPanel.js:482
msgid "Home Template"
msgstr ""

#: js/AuthenticationPanel.js:388 js/AuthenticationPanel.js:541
msgid "Host"
msgstr "Dominio"

#: js/AuthenticationPanel.js:438 js/ConfigManagerPanel.js:229
#: js/ConfigManagerPanel.js:317 js/ConfigManagerPanel.js:337
#: js/ConfigManagerPanel.js:378 js/ConfigManagerPanel.js:437
#: js/EmailPanel.js:182 js/EmailPanel.js:292 js/EmailPanel.js:396
msgid "Hostname"
msgstr "Nombre del Dominio"

#: js/ConfigManagerPanel.js:519
msgid "Hyperlink/URL"
msgstr ""

#: js/TermsPanel.js:30
msgid "I have read the license agreement and accept it"
msgstr "He leído los términos de la licencia y estoy de acuerdo con ellos"

#: js/TermsPanel.js:56
msgid "I have read the privacy agreement and accept it"
msgstr "He leído la política de privacidad y estoy de acuerdo con ella"

#: js/EnvCheckGridPanel.js:189
msgid "Ignore setup tests"
msgstr "Ignorar pruebas de instalación"

#: js/EmailPanel.js:174
msgid "Imap"
msgstr "Imap"

#: js/AuthenticationPanel.js:308
msgid "Initial Admin User"
msgstr ""

#: js/AuthenticationPanel.js:326
msgid "Initial admin Password"
msgstr "Contraseña inicial de ingreso de administrador"

#: js/AuthenticationPanel.js:321
msgid "Initial admin login name"
msgstr "Nombre inicial de ingreso de administrador"

#: js/ApplicationGridPanel.js:78
msgid "Install application"
msgstr "Instalar aplicación"

#: js/ApplicationGridPanel.js:66
msgid "Installed Version"
msgstr "Versión Instalado"

#: js/AuthenticationPanel.js:630
msgid "LDAP Master Admin"
msgstr ""

#: js/AuthenticationPanel.js:633
#, fuzzy
msgid "LDAP Master Admin Password"
msgstr "Contraseña inicial de ingreso de administrador"

#: js/AuthenticationPanel.js:627
msgid "LDAP Master Server"
msgstr ""

#: js/AuthenticationPanel.js:661
#, fuzzy
msgid "LDAP Password Expiration Policy"
msgstr "Confirmación de contraseñ"

#: js/TermsPanel.js:37
msgid "License Agreement"
msgstr "Términos de Licenciamiento"

#: js/ConfigManagerPanel.js:281 js/ConfigManagerPanel.js:398
msgid "Lifetime (seconds)"
msgstr "Tiempo de vida (segundos)"

#: js/ConfigManagerPanel.js:253
msgid "Logging"
msgstr "Registros de sucesos"

#: js/EmailPanel.js:309
msgid "Login"
msgstr "Ingreso"

#: js/AuthenticationPanel.js:395 js/AuthenticationPanel.js:544
msgid "Login name"
msgstr "Nombre de ingreso"

#: js/AuthenticationPanel.js:706
#, fuzzy
msgid "Login panel"
msgstr "Nombre de ingreso"

#: js/EmailPanel.js:496
msgid "MD5-CRYPT"
msgstr ""

#: js/ConfigManagerPanel.js:483
msgid "Map panel"
msgstr ""

#: js/AuthenticationPanel.js:605
msgid "Max Group Id"
msgstr "Id de Grupo Máximo de Usuario"

#: js/AuthenticationPanel.js:599
msgid "Max User Id"
msgstr "ID Máximo de Usuario"

#: js/ConfigManagerPanel.js:529
msgid "Maximum allowed message size (bytes)"
msgstr ""

#: js/AuthenticationPanel.js:602
msgid "Min Group Id"
msgstr "Id de Grupo Mínimo de Usuario"

#: js/AuthenticationPanel.js:596
msgid "Min User Id"
msgstr "Id Mínimo de Usuario"

#: js/AuthenticationPanel.js:762
msgid "Minimum length"
msgstr ""

#: js/AuthenticationPanel.js:774
msgid "Minimum numbers"
msgstr ""

#: js/AuthenticationPanel.js:771
msgid "Minimum special chars"
msgstr ""

#: js/AuthenticationPanel.js:768
msgid "Minimum uppercase chars"
msgstr ""

#: js/AuthenticationPanel.js:765
msgid "Minimum word chars"
msgstr ""

#: js/ConfigManagerPanel.js:515
msgid "Mouseover title"
msgstr ""

#: js/EmailPanel.js:450
msgid "MySql Database"
msgstr "Base de Datos MySql"

#: js/EmailPanel.js:447
msgid "MySql Hostname"
msgstr "Dominio de MySql"

#: js/EmailPanel.js:456
msgid "MySql Password"
msgstr "Contraseña MySql"

#: js/EmailPanel.js:460
msgid "MySql Port"
msgstr "Puerto MySql"

#: js/EmailPanel.js:453
msgid "MySql User"
msgstr "Usuario MySql"

#: js/ApplicationGridPanel.js:63
msgid "Name"
msgstr "Nombre"

#: js/AuthenticationPanel.js:293 js/AuthenticationPanel.js:360
#: js/AuthenticationPanel.js:404 js/AuthenticationPanel.js:553
#: js/AuthenticationPanel.js:591 js/AuthenticationPanel.js:622
#: js/AuthenticationPanel.js:722 js/AuthenticationPanel.js:746
#: js/AuthenticationPanel.js:752 js/AuthenticationPanel.js:758
#: js/AuthenticationPanel.js:779 js/AuthenticationPanel.js:800
#: js/AuthenticationPanel.js:806 js/EmailPanel.js:207 js/EmailPanel.js:267
#: js/EmailPanel.js:422
msgid "No"
msgstr "No"

#: js/AuthenticationPanel.js:447 js/EmailPanel.js:202 js/EmailPanel.js:303
#: js/EmailPanel.js:309 js/EmailPanel.js:407
msgid "None"
msgstr "Ninguno"

#: js/EmailPanel.js:329
msgid "Notification Password"
msgstr ""

#: js/EmailPanel.js:325
msgid "Notification Username"
msgstr ""

#: js/EmailPanel.js:334
msgid "Notifications local client (hostname or IP address)"
msgstr ""

#: js/EmailPanel.js:321
msgid "Notifications service address"
msgstr "Dirección del servicio de notificaciones"

#: js/AuthenticationPanel.js:757
msgid "Only ASCII"
msgstr ""

#: js/ApplicationGridPanel.js:65
msgid "Order"
msgstr "Ordenar"

#: js/EmailPanel.js:502
msgid "PLAIN"
msgstr ""

#: js/EmailPanel.js:495
msgid "PLAIN-MD5"
msgstr ""

#: js/AuthenticationPanel.js:398 js/AuthenticationPanel.js:547
#: js/ConfigManagerPanel.js:214 js/ConfigManagerPanel.js:245
msgid "Password"
msgstr "Contraseña"

#: js/EmailPanel.js:484
msgid "Password Scheme"
msgstr ""

#: js/AuthenticationPanel.js:730
msgid "Password Settings"
msgstr ""

#: js/AuthenticationPanel.js:330
msgid "Password confirmation"
msgstr "Confirmación de contraseñ"

#: js/AuthenticationPanel.js:584
msgid "Password encoding"
msgstr "Codificación de contraseña"

#: js/AuthenticationPanel.js:688
msgid "Password expiration attribute name"
msgstr ""

#: js/AuthenticationPanel.js:693
#, fuzzy
msgid "Password expiration interval"
msgstr "Confirmación de contraseñ"

#: js/AuthenticationPanel.js:846
msgid "Passwords don't match"
msgstr "Las contraseñas no coinciden"

#: js/ConfigManagerPanel.js:304 js/ConfigManagerPanel.js:423
msgid "Path"
msgstr "Ruta"

#: js/EnvCheckGridPanel.js:89
msgid "Performing Environment Checks..."
msgstr "Realizando controles de entorno..."

#: js/EmailPanel.js:309
msgid "Plain"
msgstr "Plano"

#: js/AuthenticationPanel.js:441 js/ConfigManagerPanel.js:234
#: js/ConfigManagerPanel.js:321 js/ConfigManagerPanel.js:341
#: js/ConfigManagerPanel.js:382 js/ConfigManagerPanel.js:441
#: js/EmailPanel.js:195 js/EmailPanel.js:296 js/EmailPanel.js:400
msgid "Port"
msgstr "Puerto"

#: js/ConfigManagerPanel.js:249
msgid "Prefix"
msgstr "Prefijo"

#: js/EmailPanel.js:313
msgid "Primary Domain"
msgstr ""

#: js/ConfigManagerPanel.js:266
msgid "Priority"
msgstr "Prioridad"

#: js/TermsPanel.js:63
msgid "Privacy Agreement"
msgstr "Política de Privacidad"

#: js/ConfigManagerPanel.js:349
msgid "Queue"
msgstr ""

#: js/AuthenticationPanel.js:621
msgid "Readonly access"
msgstr ""

#: js/AuthenticationPanel.js:799
msgid "Redirect Always (if No, only redirect after logout)"
msgstr ""

#: js/AuthenticationPanel.js:787
msgid "Redirect Settings"
msgstr "Configuración de redirección"

#: js/AuthenticationPanel.js:795
msgid "Redirect Url (redirect to login screen if empty)"
msgstr ""

#: js/AuthenticationPanel.js:805
msgid "Redirect to referring site, if exists"
msgstr "Redirigir al sitio de referencia, si es que existe"

#: js/AuthenticationPanel.js:156
msgid "Remove LDAP password expiration control"
msgstr ""

#: js/EnvCheckGridPanel.js:161
msgid "Result"
msgstr "Resultado"

#: js/AuthenticationPanel.js:721
msgid "Reuse last username logged"
msgstr ""

#: js/EnvCheckGridPanel.js:180
msgid "Run setup tests"
msgstr "Realizar pruebas de instalación"

#: js/EmailPanel.js:497
msgid "SHA1"
msgstr ""

#: js/EmailPanel.js:498
msgid "SHA256"
msgstr ""

#: js/EmailPanel.js:500
msgid "SHA512"
msgstr ""

#: js/EmailPanel.js:390
msgid "SIEVE"
msgstr ""

#: js/EmailPanel.js:499
msgid "SSHA256"
msgstr ""

#: js/EmailPanel.js:501
msgid "SSHA512"
msgstr ""

#: js/AuthenticationPanel.js:447 js/EmailPanel.js:202 js/EmailPanel.js:303
#: js/EmailPanel.js:407
msgid "SSL"
msgstr "SSL"

#: js/AuthenticationPanel.js:821 js/ConfigManagerPanel.js:544
msgid "Save config"
msgstr "Guardar configuración"

#: js/AuthenticationPanel.js:819
msgid "Save config and install"
msgstr "Guardar configuración e instalar"

#: js/AuthenticationPanel.js:412
msgid "Search filter"
msgstr ""

#: js/EmailPanel.js:317
msgid "Secondary Domains (comma separated)"
msgstr ""

#: js/AuthenticationPanel.js:445 js/EmailPanel.js:199 js/EmailPanel.js:300
#: js/EmailPanel.js:404
msgid "Secure Connection"
msgstr "Conexión Segura"

#: js/ConfigManagerPanel.js:390
msgid "Session"
msgstr ""

#: js/ConfigManagerPanel.js:198
msgid "Setup Authentication"
msgstr "Autentificación del Instalador"

#: js/Setup.js:58
msgid "Setup Checks"
msgstr "Pruebas de Instalación"

#: js/AuthenticationPanel.js:856 js/AuthenticationPanel.js:863
#: js/AuthenticationPanel.js:867 js/AuthenticationPanel.js:879
#: js/AuthenticationPanel.js:890 js/AuthenticationPanel.js:902
#: js/AuthenticationPanel.js:914
msgid "Should not be empty"
msgstr "No debería estar vacío"

#: js/EmailPanel.js:415
msgid "Sieve Proxy Password"
msgstr ""

#: js/EmailPanel.js:411
msgid "Sieve Proxy User"
msgstr ""

#: js/EmailPanel.js:284
msgid "Smtp"
msgstr "Smtp"

#: js/EmailPanel.js:150
msgid "Standard IMAP"
msgstr "IMAP estándar"

#: js/EmailPanel.js:160
msgid "Standard SMTP"
msgstr "SMTP estándar"

#: js/ApplicationGridPanel.js:68
msgid "Status"
msgstr "Estado"

#: js/AuthenticationPanel.js:136
msgid ""
"Switching from SQL to LDAP will delete all existing User Accounts, Groups "
"and Roles. Do you really want to switch the accounts storage backend to "
"LDAP ?"
msgstr ""
"Cambiar desde SQL a LDAP borrará todas las cuentas, cargos y grupos de "
"usuario. ¿Realmente desea cambiar el almacenamiento de cuentas "
"administrativo a LDAP?"

#: js/AuthenticationPanel.js:447 js/EmailPanel.js:202 js/EmailPanel.js:303
#: js/EmailPanel.js:407
msgid "TLS"
msgstr "TLS"

#: js/ConfigManagerPanel.js:457
msgid "Temporary Files Path"
msgstr "Ruta de Archivos Temporales"

#: js/ConfigManagerPanel.js:449
msgid "Temporary files"
msgstr "Archivos temporales"

#: js/Setup.js:53
msgid "Terms and Conditions"
msgstr "Términos y Condiciones"

#: js/ConfigManagerPanel.js:511
msgid "Text label"
msgstr ""

#: js/ApplicationGridPanel.js:240
msgid "This may take a while"
msgstr "Puede tomar algún tiempo"

#: js/AuthenticationPanel.js:359
msgid "Try to split username"
msgstr ""

#: js/ConfigManagerPanel.js:497
msgid "URL To Report Bugs"
msgstr ""

#: js/ApplicationGridPanel.js:87
msgid "Uninstall application"
msgstr "Desinstalar aplicación"

#: js/ApplicationGridPanel.js:96
msgid "Update application"
msgstr "Actualizar aplicación"

#: js/AuthenticationPanel.js:590
msgid "Use Rfc 2307 bis"
msgstr "Usar Rfc 2307 bis"

#: js/EmailPanel.js:266 js/EmailPanel.js:421
msgid "Use proxy auth"
msgstr ""

#: js/EmailPanel.js:206
msgid "Use system account"
msgstr ""

#: js/ConfigManagerPanel.js:241
msgid "User"
msgstr "Usuario"

#: js/AuthenticationPanel.js:558
msgid "User DN"
msgstr "Usuario DN"

#: js/AuthenticationPanel.js:561
msgid "User Filter"
msgstr ""

#: js/AuthenticationPanel.js:565
msgid "User Search Scope"
msgstr ""

#: js/AuthenticationPanel.js:611
msgid "User UUID Attribute name"
msgstr "Nombre del atributo del usuario UUID"

#: js/AuthenticationPanel.js:745
msgid "User can change password"
msgstr "Usuario puede cambiar su contraseña"

#: js/EmailPanel.js:476
msgid "User or UID"
msgstr ""

#: js/ConfigManagerPanel.js:205
msgid "Username"
msgstr "Nombre de usuario"

#: js/AuthenticationPanel.js:293 js/AuthenticationPanel.js:360
#: js/AuthenticationPanel.js:404 js/AuthenticationPanel.js:553
#: js/AuthenticationPanel.js:591 js/AuthenticationPanel.js:622
#: js/AuthenticationPanel.js:722 js/AuthenticationPanel.js:746
#: js/AuthenticationPanel.js:752 js/AuthenticationPanel.js:758
#: js/AuthenticationPanel.js:779 js/AuthenticationPanel.js:800
#: js/AuthenticationPanel.js:806 js/EmailPanel.js:207 js/EmailPanel.js:267
#: js/EmailPanel.js:422
msgid "Yes"
msgstr "Si"

#: js/ConfigManagerPanel.js:485
msgid "disabled"
msgstr ""

#: js/ConfigManagerPanel.js:485
msgid "enabled"
msgstr ""

#: js/ApplicationGridPanel.js:180
msgid "uninstall"
msgstr "desinstalar"

#: js/AuthenticationPanel.js:694
msgid "{0} days"
msgstr ""
