<?php
/**	
 * Generalization for customized queries
 *	
 * @package Tinebase
 * @license http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author Flávio Gomes da Silva Lisboa <flavio.lisboa@serpro.gov.br>
 * @copyright Copyright (c) 2013 Metaways Infosystems GmbH (http://www.metaways.de)
 */

 /**
 * Generalization for customized queries
 *
 * @package Tinebase
 */
class Tinebase_Backend_Sql_Query_Abstract 
{
	/**
	 *
	 * @var Zend_Db_Adapter_Abstract
	 */
	protected $_db;
	/**
	 *
	 * @param Zend_Db_Adapter_Abstract $db
	 */
	public function __construct(Zend_Db_Adapter_Abstract $db)
	{
		$this->_db = $db;
	}	
}