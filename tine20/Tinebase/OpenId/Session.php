<?php
/**
 * Tine 2.0
 *
 * @package     OpenId
 * @subpackage  Session
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Guilherme Striquer Bisotto <guilherme.bisotto@serpro.gov.br>
 * @copyright   Copyright (c) 2009-2011 Metaways Infosystems GmbH (http://www.metaways.de)
 *
 */

/**
 * class for Session and Session Namespaces in Core
 *
 * @package     OpenId
 * @subpackage  Session
 */
class Tinebase_OpenId_Session extends Tinebase_Session_Abstract
{
}
