/*
 * Tine 2.0
 *
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Marcelo Teixeira <marcelo.teixeira@serpro.gov.br>
 * @copyright   Copyright (c) 2007-2008 Metaways Infosystems GmbH (http://www.metaways.de)
 *
 */

Ext.ns('Tine.Tinebase');

/**
 * Local storage state provider.
 *
 * @namespace   Tine.Tinebase
 * @class       Tine.Tinebase.LocalStorageProvider
 * @extends     Ext.state.Provider
 */

Tine.Tinebase.LocalStorageProvider = function(config) {
    Tine.Tinebase.LocalStorageProvider.superclass.constructor.call(this);
    Ext.apply(this, config);
    // get all items from localStorage
    this.state = this.readLocalStorage();
};

Ext.extend(Tine.Tinebase.LocalStorageProvider, Ext.state.Provider, {
    namePrefix: 'ys-',

    set: function(name, value) {
        if (typeof value == "undefined" || value === null) {
            this.clear(name);
            return;
        }
        // write to localStorage
        localStorage.setItem(this.namePrefix + name, this.encodeValue(value));
        Tine.Tinebase.LocalStorageProvider.superclass.set.call(this, name, value);
    },
    clear: function(name) {
        localStorage.removeItem(this.namePrefix + name);
        Tine.Tinebase.LocalStorageProvider.superclass.clear.call(this, name);
    },

    readLocalStorage: function() {
        var data = {},
            i = 0;
        for (i = 0; i <= localStorage.length - 1; i++) {
            var name = localStorage.key(i);
            if (name && name.substring(0, this.namePrefix.length) == this.namePrefix) {
                data[name.substr(this.namePrefix.length)] = this.decodeValue(localStorage.getItem(name));
            }
        }
        return data;
    }
});

Tine.Tinebase.LocalStorageProvider.hasLocalStorage = function(){
    var test = 'tine';
    try {
        localStorage.setItem(test, test);
        localStorage.removeItem(test);
        return true;
    } catch(e) {
        return false;
    }
};
