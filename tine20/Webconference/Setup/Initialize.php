<?php
/**
 * Tine 2.0
 *
 * @package     Setup
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Edgar de Lucca <edgar.lucca@serpro.gov.br>, Marcelo Teixeira <marcelo.teixeira@serpro.gov.br>
 * @copyright   Copyright (c) 2008-2012 Metaways Infosystems GmbH (http://www.metaways.de)
 *
 */

/**
 * class for Webconference initialization
 *
 * @package     Setup
 */
class Webconference_Setup_Initialize extends Setup_Initialize
{
   /**
     * init favorites
     */
    protected function _initializeFavorites()
    {
        $pfe = new Tinebase_PersistentFilter_Backend_Sql();
        
        $commonValues = array(
            'account_id'        => NULL,
            'application_id'    => Tinebase_Application::getInstance()->getApplicationByName('Webconference')->getId(),
            'model'             => 'Webconference_Model_RoomFilter',
        );
        
        $myEventsPFilter = $pfe->create(new Tinebase_Model_PersistentFilter(array_merge($commonValues, array(
            'name'              => 'All my rooms' ,
            'description'       => "All Rooms I attend", // _("All events I attend")
            'filters'           => array(
                array('field' => 'status', 'operator' => 'in', 'value' => array(
                    'A'
                )),
                array('field' => 'attender'    , 'operator' => 'equals', 'value' => array(
                    'user_type' => Webconference_Model_Attender::USERTYPE_USER,
                    'user_id'   => Addressbook_Model_Contact::CURRENTCONTACT,
                ))
            )
        ))));
        
        $pfe->create(new Tinebase_Model_PersistentFilter(array_merge($commonValues, array(
            'name'              => "I'm organizer",
            'description'       => "Rooms I'm the organizer of",
            'filters'           => array(
                array('field' => 'status', 'operator' => 'in', 'value' => array(
                    'A'
                )),
                array('field' => 'organizer',
                     'operator' => 'equals', 'value' => Addressbook_Model_Contact::CURRENTCONTACT
                )
            )
        ))));
    }
 
    protected function _initializeKeyFields()
    {
        $wconfRolesConfig = array(
            'name'    => Webconference_Config::ATTENDEE_ROLES,
            'records' => array(
                array('id' => 'MODERATOR', 'value' => 'Moderator', 'system' => true),
                array('id' => 'ATTENDEE', 'value' => 'Attendee', 'system' => true),
            ),
        );

        Webconference_Config::getInstance()->set(Webconference_Config::ATTENDEE_ROLES, $wconfRolesConfig);

        $roomStatusConfig = array(
            'name'    => Webconference_Config::ROOM_STATUS,
            'records' => array(
                array('id' => 'A', 'value' => 'Active', 'system' => true),
                array('id' => 'E', 'value' => 'Expired', 'system' => true),
            ),
        );

        Webconference_Config::getInstance()->set(Webconference_Config::ROOM_STATUS, $roomStatusConfig);
    }
}