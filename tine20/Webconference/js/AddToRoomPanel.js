/*
 * Tine 2.0
 * 
 * @package     Webconference
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Edgar de Lucca <edgar.lucca@serpro.gov.br>
 * @copyright   Copyright (c) 2009-2012 Metaways Infosystems GmbH (http://www.metaways.de)
 *
 */

Ext.ns('Tine.Webconference');

/**
 * @namespace   Tine.Webconference
 * @class       Tine.Webconference.AddToRoomPanel
 * @extends     Tine.widgets.dialog.AddToRecordPanel
 * @author      Edgar de Lucca <edgar.lucca@serpro.gov>
 */
Tine.Webconference.AddToRoomPanel = Ext.extend(Tine.widgets.dialog.AddToRecordPanel, {
    appName: 'Webconference',
    recordClass: Tine.Webconference.Model.Room,
    callingApp: 'Addressbook',
    callingModel: 'Contact',
    
    /**
     * @see Tine.widgets.dialog.AddToRecordPanel::isValid()
     */
    isValid: function() {
        var valid = true;
        if(this.searchBox.getValue() == '') {
            this.searchBox.markInvalid(this.app.i18n._('Please choose the Event to add the contacts to'));
            valid = false;
        }
        return valid;
    },
    
    /**
     * @see Tine.widgets.dialog.AddToRecordPanel::getRelationConfig()
     */
    getRelationConfig: function() {
        var config = {
            role: this.chooseRoleBox.getValue()
        }
        return config;
    },
    
    /**
     * @see Tine.widgets.dialog.AddToRecordPanel::getFormItems()
     */
    getFormItems: function() {
        return {
            border: false,
            frame:  false,
            layout: 'border',

            items: [{
                region: 'center',
                border: false,
                frame:  false,
                layout : {
                    align: 'stretch',
                    type:  'vbox'
                    },
                items: [{
                    layout:  'form',
                    margins: '10px 10px',
                    border:  false,
                    frame:   false,
                    items: [ 
                        Tine.widgets.form.RecordPickerManager.get('Webconference', 'Room', {ref: '../../../searchBox'}),
                        {
                            fieldLabel: this.app.i18n._('Role'),
                            emptyText: this.app.i18n._('Select Role'),
                            xtype: 'widget-keyfieldcombo',
                            app:   'Webconference',
                            value: 'ATTENDEE',
                            anchor : '100% 100%',
                            margins: '10px 10px',
                            keyFieldName: 'wconfRoles',
                            ref: '../../../chooseRoleBox'
                        }
                     ] 
                }]

            }]
        };
    } 
});

Tine.Webconference.AddToRoomPanel.openWindow = function(config) {
    var window = Tine.WindowFactory.getWindow({
        modal: true,
        title : String.format(Tine.Tinebase.appMgr.get('Webconference').i18n._('Adding {0} Attendee to room'), config.count),
        width : 240,
        height : 250,
        contentPanelConstructor : 'Tine.Webconference.AddToRoomPanel',
        contentPanelConstructorConfig : config
    });
    return window;
};
