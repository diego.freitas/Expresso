/*
 * Tine 2.0
 * 
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Edgar de Lucca <edgar.lucca@serpro.gov.br>
 * @copyright   Copyright (c) 2010 Metaways Infosystems GmbH (http://www.metaways.de)
 */
Ext.ns('Tine.Webconference');

/**
 * @namespace   Tine.Webconference
 * @class       Tine.Webconference.AttendeeFilterModel
 * @extends     Tine.widgets.grid.FilterModel
 * 
 * @author      Edgar de Lucca <edgar.lucca@serpro.gov.br>
 */
Tine.Webconference.AttendeeFilterModel = Ext.extend(Tine.widgets.grid.FilterModel, {
    /**
     * @property Tine.Tinebase.Application app
     */
    app: null,
    
    field: 'attender',
    defaultOperator: 'in',
    
    /**
     * @private
     */
    initComponent: function() {
        Tine.Webconference.AttendeeFilterModel.superclass.initComponent.call(this);
        
        this.app = Tine.Tinebase.appMgr.get('Webconference');
        
        this.operators = ['in'/*, 'notin'*/];
        this.label = this.app.i18n._('Attendee');
        
        this.defaultValue = Ext.apply(Tine.Webconference.Model.Attender.getDefaultData(), {
            user_id: Tine.Tinebase.registry.get('currentAccount')
        });
    },
    
    /**
     * value renderer
     * 
     * @param {Ext.data.Record} filter line
     * @param {Ext.Element} element to render to 
     */
    valueRenderer: function(filter, el) {
        var value = new Tine.Webconference.AttendeeFilterModelValueField({
            app: this.app,
            filter: filter,
            width: 200,
            id: 'tw-ftb-frow-valuefield-' + filter.id,
            value: filter.data.value ? filter.data.value : this.defaultValue,
            renderTo: el
        });
        value.on('select', this.onFiltertrigger, this);
        return value;
    }
});

Tine.widgets.grid.FilterToolbar.FILTERS['webconference.attendee'] = Tine.Webconference.AttendeeFilterModel;

/**
 * @namespace   Tine.Webconference
 * @class       Tine.Webconference.AttendeeFilterModelValueField
 * @extends     Ext.ux.form.LayerCombo
 * 
 * @author      Cornelius Weiss <c.weiss@metaways.de>
 */
Tine.Webconference.AttendeeFilterModelValueField = Ext.extend(Ext.ux.form.LayerCombo, {
    hideButtons: false,
    layerAlign : 'tr-br?',
    minLayerWidth: 400,
    layerHeight: 300,
    
    lazyInit: true,
    
    formConfig: {
        labelAlign: 'left',
        labelWidth: 30
    },
    
    attendeeData: null,
    
    initComponent: function() {
        this.on('beforecollapse', this.onBeforeCollapse, this);
        
        this.supr().initComponent.call(this);
    },
    
    getFormValue: function() {
        return this.attendeeFilterGrid.getValue();
    },
    
    getItems: function() {
        
        this.attendeeFilterGrid = new Tine.Webconference.AttendeeFilterGrid({
            title: this.app.i18n._('Select Attendee'),
            height: this.layerHeight || 'auto',
            showNamesOnly: true,
            showMemberOfType: true,
            onStoreChange: Ext.emptyFn
        });
        this.attendeeFilterGrid.store.on({
            'add': function (store) {
                this.action_ok.setDisabled(this.attendeeFilterGrid.store.getCount() === 1);
            },
            'remove': function (store) {
                this.action_ok.setDisabled(this.attendeeFilterGrid.store.getCount() === 1);
            },
            scope: this
        });
        
        var items = [this.attendeeFilterGrid];
        
        return items;
    },
    
    /**
     * cancel collapse if ctx menu is shown
     */
    onBeforeCollapse: function() {
        
        return (!this.attendeeFilterGrid.ctxMenu || this.attendeeFilterGrid.ctxMenu.hidden) &&
                !this.attendeeFilterGrid.editing;
    },
    
    /**
     * @param {String} value
     * @return {Ext.form.Field} this
     */
    setValue: function(value) {
        value = Ext.isArray(value) ? value : [value];
        this.attendeeData = value;
        this.currentValue = [];
        
        var attendeeStore = Tine.Webconference.Model.Attender.getAttendeeStore(value);
        
        var a = [];
        attendeeStore.each(function(attender) {
            this.currentValue.push(attender.data);
            var name = Tine.Webconference.AttendeeGridPanel.prototype.renderAttenderName.call(Tine.Webconference.AttendeeGridPanel.prototype, attender.get('user_id'), false, attender);
            a.push(name);
        }, this);
        
        this.setRawValue(a.join(', '));
        return this;
        
    },
    
    /**
     * sets values to innerForm
     */
    setFormValue: function(value) {
        this.attendeeFilterGrid.setValue(this.attendeeData);
    }
});
